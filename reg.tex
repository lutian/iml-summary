\section{Regression}\label{sec:reg}

\textbf{Linear Regression}: Approximate $y$ as $w^T x + w_0$ for some $x, w \in
\Rd$. We write $\tilde{w} = \begin{bmatrix}w & w_0\end{bmatrix}^T$ and
    $\tilde{x} = \begin{bmatrix}x & 1\end{bmatrix}^T$

Quantify goodness-of-fit via \emph{residuals}: $r_i = y_i - w^T x_i$

\begin{tabular}{ll}
    2-norm:& $\hat{R}(w) = \sum_{i=1}^n (y_i - w^T x_i)^2 = \norm{Xw-y}^2_2$ \\
    1-norm:& $\hat{R}(w) = \sum_{i=1}^n \lvert y_i - w^T x_i \rvert = \norm{Xw - y}_1$\\
\end{tabular}

Solve optimization problem to determine optimal $w^*$:

\centerline{$w^* = \argmin_w \sum_{i=1}^n \hat{R}(w)$}

For sum of squared residuals we have a closed form
\centerline{$w^* = (X^T X)^{-1} X^T y$}
if $X^T X$ has full rank.

\textbf{Gradient descent}: Small adjustments to weights in order to achieve
maximum with \emph{learning rate} $\eta_t$:

\begin{enumerate}
    \item Start at $w_0 \in \Rd$
    \item For $t = 0, 1, \dots$ do $w_{t+1} = w_t - \eta_t \nabla \hat{R}(w_t)$
\end{enumerate}

Finds optimal solution when problem is \emph{convex}.

Squared loss: Converges \emph{linearly} with step size $\frac{1}{2}$.

One iteration: $\mathcal{O}(nd)$, constant \# of iterations

\green{Better complexity} than closed form, may \green{not need optimal
solution}, many problems have \green{no closed form}

\textbf{Nonlinear reg.}: apply feature transform $\phi$ before regression:

\centerline{$f(x) = \sum_{i=1}^n w_i \tilde{x} = \sum_{i=1}^n w_i \phi_i(x)$}

Fundamental assumption: Data set is generated \emph{independently} and
\emph{identically distributed} from some unknown distribution $(x_i, y_i) \sim
P(X, Y)$. We want to minimize \emph{true risk}.

\centerline{$R(w) = \int P(x, y)(y - w^T x)^2 dx dy = \E_{x,y}\left[(y - w^T x)^2\right]$}

\textbf{True risk}: Can estimate on sample $D$, as $\hat{R}_D(w) \rightarrow R(w)$ for $w$
fixed and $\lvert D\rvert \rightarrow \infty$.

\centerline{$\hat{R}_D(w) = \frac{1}{\lvert D\rvert} \sum_{(x, y) \in D} (y -
w^T x)^2$}

We \red{\emph{underestimate}} true risk: $\E[\hat{R}_D(w)] < \E[R(w)]$.
Use independent test sets, leads to \emph{cross validation}.

$k$-fold CV: Split data into $k$ equal folds. Train on $k-1$ folds,
estimate performance on last fold. Pick best model from $k$ folds.
In practice $k = 5, 10$. $k = n-1$ \green{good results} but \red{slow}.

\subsection{Regularization}

Models can be very complex if we only minimize loss. Using \emph{regularizers}
we constrain weights to be small.
\begin{tabular}{rlrl}
    \hspace{2em}    Ridge: &$\lambda\norm{w}_2^2$ &LASSO: &$\lambda\norm{w}_1$
\end{tabular}

Regularized optimization problem:

\centerline{$\min_w \frac{1}{n} \sum_{i = 1}^n (y_i - w^T x_i)^2 + \lambda \norm{w}_2^2$}

Scale of $X$ matters, standardize data $(\tilde{x_i} = \frac{x_i -
\mu_i}{\sigma_i})$ before solving. Still has analytical solution, but can use
gradient descent.

\centerline{$\tilde{w} = (X^T X + n\lambda I)^{-1}X^T y$}

\centerline{$\nabla_w \tilde{R}(w) = \frac{1}{n} \nabla_w{\hat{R}(w)} + 2 w$}
